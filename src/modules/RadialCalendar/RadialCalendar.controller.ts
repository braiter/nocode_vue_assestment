import {defineComponent, onMounted} from 'vue'
import { ref } from 'vue'
import axios from 'axios'
import RadialCalendarItems from "@/modules/RadialCalendarItems/RadialCalendarItems.vue";
import moment from 'moment'
import type { Ref } from 'vue'

interface OrbitI {
    contact_date: string;
    array: Array<OrbI>
}

interface OrbI {
    city: string;
    created_at: number;
    id: number;
    img: string;
    name: string
    position: string
    _orbits_last_message : {
        message: string;
        message_head: string;
    }
}

export default defineComponent({
    name: 'RadialCalendar',
    components: {RadialCalendarItems},
    setup() {
        const circles: Ref<Array<OrbitI>> = ref([]);
        const visibleCirclesAmount: Ref<number> = ref(0);
        const visibleCircleIndex: Ref<number> = ref(0);
        const currentStep: Ref<number> = ref(1);
        const radialCalendarContainer: Ref<HTMLDivElement>|Ref<null> = ref(null);
        const radius: Ref<number> = ref(0);
        const radiusStep:Ref<number> = ref(0);
        const loading: Ref<boolean> = ref(false);

        let debounceTimer: ReturnType<typeof setTimeout>|undefined;

        onMounted(async (): Promise<void> => {
            let date = getDate(new Date().getTime());

            await getData(date);
            visibleCirclesAmount.value = circles.value.length;

            date = circles.value[circles.value.length - 1].contact_date;

            await getData(getDate(moment(date).add(-1,'days').toString()));

            const body = document.body;

            radius.value = Math.min(body.offsetHeight, body.offsetWidth)/visibleCirclesAmount.value - 20;

            document.addEventListener("wheel", (event) => {
                debounceScroll(event.deltaY < 0);
            });
        })

        function getDate(date: string|number) : string {
            const formattedDate = new Date(date);
            return `${formattedDate.getFullYear()}-${formattedDate.getMonth() + 1}-${formattedDate.getDate()}`;
        }

        async function debounceScroll(up: boolean): Promise<void> {
            if (!debounceTimer) {
                if (up) {
                    currentStep.value++;
                    visibleCircleIndex.value--;

                    if (visibleCirclesAmount.value + currentStep.value == circles.value.length - 1 && !loading.value) {
                        const lastDay = circles.value[circles.value.length - 1].contact_date;
                        const date = getDate(moment(lastDay).add(-1,'days').toString())

                        getData(date)
                    }
                } else if (currentStep.value > 1) {
                    currentStep.value--;

                    visibleCircleIndex.value++;

                }
            }

            clearTimeout(debounceTimer);
            debounceTimer = setTimeout(() => {
                debounceTimer = undefined;
            }, 200);
        }

        async function getData(date: string) {
            loading.value = true;
            const res = await axios.get(`https://xsrr-l2ye-dpbj.f2.xano.io/api:oUvfVMO5/receive_week?start_date=${date}`);
            loading.value = false;

            circles.value = circles.value.concat(res.data);
        }

        return {
            circles,
            visibleCirclesAmount,
            visibleCircleIndex,
            currentStep,
            radialCalendarContainer,
            radiusStep,
            radius,
            loading
        }
    },
    methods: {
        formatDate(date: string) {
            return moment(date).calendar(null, {
                sameDay: '[Today]',
                nextDay: '[Tomorrow]',
                nextWeek: 'dddd',
                lastDay: '[Yesterday]',
                lastWeek: 'ddd, MMMM D',
                sameElse: 'ddd, MMMM D, YYYY'
            });
        }
    },
    computed: {
        getCirlcesRange() {
            if (this.circles.length) {
                return this.circles.slice(0, this.visibleCirclesAmount + this.currentStep);
            }
            return [];
        }
    }
})

