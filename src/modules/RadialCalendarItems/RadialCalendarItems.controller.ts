import {defineComponent} from 'vue'
import type { PropType } from 'vue'
import { ref } from 'vue'
import moment from 'moment'

interface OrbitI {
    contact_date: string;
    array: Array<OrbI>;
}

interface OrbI {
    city: string;
    created_at: number;
    id: number;
    img: string;
    name: string;
    position: string;
    expandedMessage: boolean;
    _orbits_last_message : {
        message: string;
        message_head: string;
    }
}

export default defineComponent({
    name: 'RadialCalendarItem',
    props: {
        circle: {
            type: Object as PropType<OrbitI>
        },
        circleIndex: Number,
        circleRadius: Number,
    },
    setup(props) {
        const users = ref(props.circle? props.circle.array: []);

        return {
            users
        }
    },
    methods: {
        getUserPosition(index: number): {xPos: number, yPos: number} {

            const angle: number =  (140 / (this.users.length + 1) * (index + 1) * -1) - 20;
            const radius: number = this.circleRadius || 1;

            const xPos: number = radius + (radius * Math.cos(angle * Math.PI / 180)) - 30;
            const yPos: number = radius + (radius * Math.sin(angle * Math.PI / 180)) - 30;


            return {xPos, yPos}
        },
        isVerticalReversed(itemIndex: number) {
            const circleIndex: number = this.circleIndex || 0;

            if (circleIndex < 3) {
                return true;
            }
            else if (circleIndex < 7) {
                if (this.users.length < 7 && this.users.length > 3 && (itemIndex == 0 || itemIndex == this.users.length - 1)) {
                    return true;
                }
                else if (this.users.length == 7 && (itemIndex < 3 || itemIndex > this.users.length - 4)) {
                    return true;
                }
                else if (this.users.length >= 8 && (itemIndex < 4 || itemIndex > this.users.length - 5)) {
                    return true;
                }
            }
            else {
                if ((this.users.length == 6 || 7) && (itemIndex == 0 || itemIndex == this.users.length - 1)) {
                    return true;
                }
                else if ((this.users.length >= 8 && this.users.length <= 10) && (itemIndex < 2 || itemIndex > this.users.length - 2)) {
                    return true;
                }
                else if (this.users.length >= 10 && (itemIndex < 3 || itemIndex > this.users.length - 4)) {
                    return true;
                }
            }
        },
        formatDate(date: number, fromNow?: boolean) {
            if (fromNow) {
                return moment(date).fromNow();
            }
            return moment(date).format('LLLL');
        }
    }
})